package com.ucbcba.blog.controllers;

import com.ucbcba.blog.entities.Comment;
import com.ucbcba.blog.entities.Post;
import com.ucbcba.blog.services.CommentService;
import com.ucbcba.blog.services.PostService;
import com.ucbcba.blog.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

/**
 * Created by amolina on 26/09/17.
 */
@Controller
public class CommentController {

    private CommentService commentService;
    private UserService userService;


    @Autowired
    public void  setUserService(UserService userService){this.userService=userService;}
    @Autowired
    public void setCommentService(CommentService commentService) {
        this.commentService = commentService;
    }

    /**
     * List all posts.
     *
     * @param model
     * @return
     */
    @RequestMapping(value = "/comment", method = RequestMethod.POST)
    public String save(@Valid Comment comment, Model model) {
        commentService.saveComment(comment);
        model.addAttribute("users",userService.listAllUsers());
        return "redirect:/post/"+comment.getPost().getId();
    }


    @RequestMapping(value = "/comment/like/{id}", method = RequestMethod.GET)
    public String like(@PathVariable Integer id, Model model) {
        Comment comment = commentService.getCommentById(id);
        comment.setLikes(comment.getLikes()+1);
        commentService.saveComment(comment);
        return "redirect:/post/"+comment.getPost().getId();
    }
    @RequestMapping(value = "/comment/dislike/{id}", method = RequestMethod.GET)
    public String dislike(@PathVariable Integer id, Model model) {
        Comment comment = commentService.getCommentById(id);
        if (comment.getLikes()>0){
            comment.setLikes(comment.getLikes()-1);
            commentService.saveComment(comment);
        }

        return "redirect:/post/"+comment.getPost().getId();
    }

}
